package org.tzeng;

/**
 * Hello world!
 *
 */
public class App {
    public static void main( String[] args ) {
        String str = "Hello com.World! he(is)ll";

        System.out.println( str.replaceAll("\\)", " ").replaceAll("\\(", " ").replaceAll("\\.", " ") );
    }
}

package org.tzeng.glwj.datag;

/**
 * Created by tzeng on 2016-04-01.
 */
public class Range {
    private int start;
    private int end;
    private int size;

    public Range(int start, int end) {
        this.start = start;
        this.end = end;
    }

    public Range(int start, int end, int size) {
        this.start = start;
        this.end = end;
        this.size = size;
    }

    public int getStart() {
        return start;
    }

    public void setStart(int start) {
        this.start = start;
    }

    public int getEnd() {
        return end;
    }

    public void setEnd(int end) {
        this.end = end;
    }
    public int getSize() {
        return size;
    }

    public void setSize(int size) {
        this.size = size;
    }
}

package org.tzeng.glwj.datag;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;


public class GenerateDataSet {
	DataSet dataSet;

	public GenerateDataSet() {
		dataSet = new DataSet();
	}

	public static void main(String[] args) {
		GenerateDataSet generate = new GenerateDataSet();
		DataSet dataSet  = generate.GetDataSet(9, 10, 20, 300, 30);
		ArrayList<Item> items = dataSet.getDatas();
		for(int i = 0,size = items.size();i<size;i++){
			System.out.println(items.get(i).getContent() + "\t : \t["+ items.get(i).getStart() +", "+items.get(i).getEnd() + "]");
		}
	}

    public List<Item> GenerateDB(int conentLength,int charNum,int lowLimit,int highLimit,int itemsNum){
        return GetDataSet(conentLength, charNum, lowLimit, highLimit, itemsNum).getDatas();
    }

	public DataSet GetDataSet(int conentLength,int charNum,int lowLimit,int highLimit,int itemsNum){
		if(charNum<2){
			System.out.println("字符数量不合法！");
		}
		if(lowLimit>=highLimit||lowLimit<1){
			System.out.println("时间约束不合法！");
		}
		if(itemsNum<1){
			System.err.println("项目集数量不合法！");
		}
		ArrayList<Item> items = dataSet.getDatas();
		Random itemSize = new Random();
		Random itemContent = new Random(System.currentTimeMillis()-12321);
		Random startTime = new Random(System.currentTimeMillis());
		Random endTime = new Random(System.currentTimeMillis()+12321);
		for (int i = 0; i < itemsNum; i++) {
			Item item = new Item();
			int size = itemSize.nextInt(conentLength);
			int start = startTime.nextInt(highLimit-lowLimit)+lowLimit;
			int end = startTime.nextInt(highLimit-lowLimit)+lowLimit;
			StringBuffer sb = new StringBuffer();
			if(size==0)
				size = 1;
			for (int j = 0; j < size; j++) {
				char c = (char)(itemContent.nextInt(charNum)+'a');
				while(sb.indexOf(""+c)!=-1){
					c = (char)(itemContent.nextInt(charNum)+'a');
				}
				sb.append(c);
			}
			if(start>end){
				int temp;
				temp = start;
				start = end;
				end = temp;
			}else if(start==end){
				end+=1;
			}
			char[] chars = new char[sb.length()];
			sb.toString().getChars(0, sb.length(), chars, 0);
			Arrays.sort(chars);
			item.setContent(new String(chars));
			item.setStart(start);
			item.setEnd(end);
			items.add(item);
		}
		return dataSet;
	}
}

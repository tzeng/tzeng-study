package org.tzeng.glwj.datag;

import java.util.ArrayList;


public class DataSet {
	int length;
	ArrayList<Item> datas;
	public DataSet() {
		datas = new ArrayList<Item>();
		length = 0;
	}
	public int getLength() { return datas.size(); }
	public void setLength(int length) {
		this.length = length;
	}
	public ArrayList<Item> getDatas() {
		return datas;
	}
	public void setDatas(ArrayList<Item> datas) {
		this.datas = datas;
	}


}

package org.tzeng.glwj.datag;

public class Item {
    private int start;
    private int end;
    private String content;

    public Item() {
        super();
    }

    public Item(int start, int end, String content) {
        super();
        this.start = start;
        this.end = end;
        this.content = content;
    }

    public int getStart() {
        return start;
    }

    public void setStart(int start) {
        this.start = start;
    }

    public int getEnd() {
        return end;
    }

    public void setEnd(int end) {
        this.end = end;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

}

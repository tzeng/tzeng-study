package org.tzeng.glwj.datag;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by tzeng on 2016-04-01.
 */
public class ItemRanges {
    private String keystr;
    private List<Range> ranges;


    public ItemRanges(String keystr, Range range) {
        List<Range> rs = new ArrayList<Range>();
        rs.add(range);
        this.keystr = keystr;
        this.ranges = rs;
    }



    public String getKeystr() {
        return keystr;
    }

    public void setKeystr(String keystr) {
        this.keystr = keystr;
    }

    public List<Range> getRanges() {
        return ranges;
    }

    public void setRanges(List<Range> ranges) {
        this.ranges = ranges;
    }
}

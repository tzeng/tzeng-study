package org.tzeng.glwj.datag;

import java.util.ArrayList;


public class TestGenerate {
	public static void main(String[] args) {
		GenerateDataSet generate = new GenerateDataSet();
		DataSet dataSet  = generate.GetDataSet(9,10, 20, 300, 30);
		ArrayList<Item> items = dataSet.getDatas();
		for(int i = 0,size = items.size();i<size;i++){
			System.out.println(items.get(i).getContent() + "\t : \t["+ items.get(i).getStart() +", "+items.get(i).getEnd() + "]");
		}
	}
}

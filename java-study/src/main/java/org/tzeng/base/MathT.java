package org.tzeng.base;

/**
 * Created by tzeng on 2016-02-26.
 */
public class MathT {
    /**
     * 欧几里得算法，求 p,q 的最大公约数
     * @param p
     * @param q
     * @return
     */
    public static int gcd(int p, int q){
        if (q == 0) return p;
        int r = p % q;
        return gcd(q, r);
    }
}

package org.tzeng.commons.lang;

import org.apache.commons.lang.CharSet;
import org.apache.commons.lang.CharSetUtils;
import org.apache.commons.lang.math.NumberUtils;
import org.junit.Assert;
import org.junit.Test;

/**
 * Created by tzeng on 2016-01-14.
 */
public class StringUtilsTest {
    @Test
    public void TestDDD(){
        CharSet charSet = CharSet.getInstance("aeiou");
        String demoStr = "The quick brown fox jumps over the lazy dog.";

        int count = 0;
        for(int i = 0, strlen = demoStr.length(); i < strlen; i ++){
            if (charSet.contains(demoStr.charAt(i)))
                count ++;
        }

        Assert.assertEquals("char set is 11", 11, count);

        Assert.assertEquals(CharSetUtils.count("The quick brown fox jumps over the lazy dog.", "aeiou"), 11);

        System.out.println(NumberUtils.isNumber("0123.1"));
    }
}

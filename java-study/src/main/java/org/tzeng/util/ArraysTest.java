package org.tzeng.util;

import org.junit.Assert;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;

/**
 * Created by tzeng on 2016-01-14.
 */
public class ArraysTest {

    /**
     * public static <T> List<T> asList(T... a)
     */
    @Test
    public void asListTest() {
        /*
         * Arrays.asList()用法
         */
        List<String> stooges = Arrays.asList("Larry", "Moe", "Curly");
        Assert.assertEquals(stooges.size(), 3);

        /*
         * 期望的输出是 list里面也有4个元素，也就是size为4，然而结果是1.
         * int[] 是一个类型，如果要传可变参数，可用其对应的包装类声明变量类型，即Integer[]
         */
        int[] a1 = {1, 2, 3, 4};
        List list1 = Arrays.asList(a1);
        Assert.assertEquals(list1.size(), 1);

        /*
         * Integer[] 是Integer类型的数组
         */
        Integer[] a4 = {1, 2, 3, 4};
        List list4 = Arrays.asList(a4);
        Assert.assertEquals(list4.size(), 4);

        //创建一个真正的ArrayList，应该用下面这种方式
        String[] arr = {"Larry", "Moe", "Curly"};
        ArrayList<String> aList = new ArrayList<String>(Arrays.asList(arr));
        Assert.assertEquals(aList.size(), 3);

        // 检测一个Array是否包含一个元素
        Assert.assertTrue(Arrays.asList(arr).contains("Moe"));
        Assert.assertFalse(Arrays.asList(arr).contains("Moea"));
        Assert.assertFalse(Arrays.asList(arr).contains(""));
        Assert.assertFalse(Arrays.asList(arr).contains(" "));
        Assert.assertFalse(Arrays.asList(arr).contains(null));

        // 或者用下面方法
        Assert.assertTrue(contain(arr, "Moe"));
        Assert.assertFalse(contain(arr, "Moea"));
        Assert.assertFalse(contain(arr, ""));
        Assert.assertFalse(contain(arr, null));
    }

    // 检测一个Array是否包含一个元素
    public boolean contain(String[] arr, String target) {
        for (String s : arr) {
            if (s.equals(target))
                return true;
        }
        return false;
    }

    /**
     * remove one element from list
     */
    @Test
    public void removeTest() {
        ArrayList<String> list = new ArrayList<String>(Arrays.asList("a", "b", "c", "d"));

        Iterator<String> inter = list.iterator();
        while (inter.hasNext()) {
            String s = inter.next();
            if (s.equals("a")) {
                inter.remove();
            }
        }

        Assert.assertEquals(list, new ArrayList<String>(Arrays.asList("b", "c", "d")));
    }
}

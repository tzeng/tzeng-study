package org.tzeng.util;

import org.apache.log4j.Logger;
import org.junit.Test;

/**
 * Created by tzeng on 2016-01-14.
 */
public class TestLog {
    private static Logger logger = Logger.getLogger(TestLog.class);

    @Test
    public void testLog(){
        // 记录debug级别的信息
        logger.debug("This is debug message.");
        // 记录info级别的信息
        logger.info("This is info message.");
        // 记录error级别的信息
        logger.error("This is error message.");
    }
}
